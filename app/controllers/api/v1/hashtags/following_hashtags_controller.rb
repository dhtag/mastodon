# frozen_string_literal: true

class Api::V1::Hashtags::FollowingHashtagsController < Api::BaseController
  before_action :require_user!

  respond_to :json

  def index
    render json: following_results
  end

  private

  def following_results
    hashtag_names.map do |tag|
      {
        name: tag,
        following: hashtags_followed_by_user.include?(tag),
      }
    end
  end

  def hashtag_names
    Array(params[:name]).map(&:to_s)
  end

  def hashtags_followed_by_user
    FollowHashtagService.instance.hashtags_followed_by(current_user.account).map{|tag| tag.name}
  end

end

