# frozen_string_literal: true

class Api::V1::HashtagsController < Api::BaseController
  before_action :require_user!

  respond_to :json
  def follow
    JournalService.instance.write("account #{current_user.account_id} has followed hashtag #{hashtag_name}")
    JournalService.instance.write("account #{current_user.account}")
    FollowHashtagService.instance.add_local_follower(current_user.account, hashtag_name)
    JournalService.instance.write('done!')
    head :created
  end

  def unfollow
    JournalService.instance.write("account #{current_user.account_id} has unfollowed hashtag #{hashtag_name}")
    FollowHashtagService.instance.remove_local_follower_with_tag_name(current_user.account, hashtag_name)
    head :created
  end

  private

  def hashtag
    Tag.find_or_create_by_names(hashtag_name)
  end

  def hashtag_name
    params[:id]
  end
end
