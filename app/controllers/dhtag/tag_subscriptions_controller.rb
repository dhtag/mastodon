class Dhtag::TagSubscriptionsController < ApplicationController
  before_action :set_dhtag_tag_subscription, only: [:show, :edit, :update, :destroy]

  # GET /dhtag/tag_subscriptions
  # GET /dhtag/tag_subscriptions.json
  def index
    @dhtag_tag_subscriptions = Dhtag::TagSubscription.all
  end

  # GET /dhtag/tag_subscriptions/1
  # GET /dhtag/tag_subscriptions/1.json
  def show
  end

  # GET /dhtag/tag_subscriptions/new
  def new
    @dhtag_tag_subscription = Dhtag::TagSubscription.new
  end

  # GET /dhtag/tag_subscriptions/1/edit
  def edit
  end

  # POST /dhtag/tag_subscriptions
  # POST /dhtag/tag_subscriptions.json
  def create
    @dhtag_tag_subscription = Dhtag::TagSubscription.new(dhtag_tag_subscription_params)

    respond_to do |format|
      if @dhtag_tag_subscription.save
        format.html { redirect_to @dhtag_tag_subscription, notice: 'Tag subscription was successfully created.' }
        format.json { render :show, status: :created, location: @dhtag_tag_subscription }
      else
        format.html { render :new }
        format.json { render json: @dhtag_tag_subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dhtag/tag_subscriptions/1
  # PATCH/PUT /dhtag/tag_subscriptions/1.json
  def update
    respond_to do |format|
      if @dhtag_tag_subscription.update(dhtag_tag_subscription_params)
        format.html { redirect_to @dhtag_tag_subscription, notice: 'Tag subscription was successfully updated.' }
        format.json { render :show, status: :ok, location: @dhtag_tag_subscription }
      else
        format.html { render :edit }
        format.json { render json: @dhtag_tag_subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dhtag/tag_subscriptions/1
  # DELETE /dhtag/tag_subscriptions/1.json
  def destroy
    @dhtag_tag_subscription.destroy
    respond_to do |format|
      format.html { redirect_to dhtag_tag_subscriptions_url, notice: 'Tag subscription was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dhtag_tag_subscription
      @dhtag_tag_subscription = Dhtag::TagSubscription.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def dhtag_tag_subscription_params
      params.require(:dhtag_tag_subscription).permit(:account_id, :tags_id)
    end
end
