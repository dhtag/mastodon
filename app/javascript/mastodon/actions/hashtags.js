import api from '../api';

export const HASHTAG_FOLLOW_FETCH_REQUEST = 'HASHTAG_FOLLOW_FETCH_REQUEST';
export const HASHTAG_FOLLOW_FETCH_SUCCESS = 'HASHTAG_FOLLOW_FETCH_SUCCESS';
export const HASHTAG_FOLLOW_FETCH_FAIL    = 'HASHTAG_FOLLOW_FETCH_FAIL';

export const HASHTAG_FOLLOW_REQUEST = 'HASHTAG_FOLLOW_REQUEST';
export const HASHTAG_FOLLOW_SUCCESS = 'HASHTAG_FOLLOW_SUCCESS';
export const HASHTAG_FOLLOW_FAIL = 'HASHTAG_FOLLOW_FAIL';

export const HASHTAG_UNFOLLOW_REQUEST = 'HASHTAG_UNFOLLOW_REQUEST';
export const HASHTAG_UNFOLLOW_SUCCESS = 'HASHTAG_UNFOLLOW_SUCCESS';
export const HASHTAG_UNFOLLOW_FAIL = 'HASHTAG_UNFOLLOW_FAIL';


export function fetchHashtagFollows(hashtagNames) {
  return (dispatch, getState) => {

    if (hashtagNames.length === 0) {
      return;
    }

    dispatch(fetchHashtagFollowRequest(hashtagNames));

    api(getState).get(`/api/v1/hashtags/following?${hashtagNames.map(id => `name[]=${id}`).join('&')}`).then(response => {
      dispatch(fetchHashtagFollowSuccess(response.data));
    }).catch(error => {
      dispatch(fetchHashtagFollowFail(error));
    });
  };
}

export function fetchHashtagFollowRequest(names) {
  return {
    type: HASHTAG_FOLLOW_FETCH_REQUEST,
    names,
    skipLoading: true,
  };
}

export function fetchHashtagFollowSuccess(hashtagFollows) {
  return {
    type: HASHTAG_FOLLOW_FETCH_SUCCESS,
    hashtagFollows,
    skipLoading: true,
  };
}

export function fetchHashtagFollowFail(error) {
  return {
    type: HASHTAG_FOLLOW_FETCH_FAIL,
    error,
    skipLoading: true,
  };
}

export function followHashtag(hashtagName) {
  return (dispatch, getState) => {
    dispatch(followHashtagRequest(hashtagName));

    api(getState).post(`/api/v1/hashtags/${hashtagName}/follow`).then(() => {
      dispatch(followHashtagSuccess(hashtagName));
    }).catch(error => {
      dispatch(followHashtagFail(error));
    });
  };
}

export function followHashtagRequest(hashtagName) {
  return {
    type: HASHTAG_FOLLOW_REQUEST,
    hashtagName: hashtagName,
    skipLoading: true,
  };
}

export function followHashtagSuccess(hashtagName) {
  return {
    type: HASHTAG_FOLLOW_SUCCESS,
    hashtagName: hashtagName,
    skipLoading: true,
  };
}

export function followHashtagFail(error) {
  return {
    type: HASHTAG_FOLLOW_FAIL,
    error,
    skipLoading: true,
  };
}

export function unfollowHashtag(hashtagName) {
  return (dispatch, getState) => {
    dispatch(unfollowHashtagRequest(hashtagName));

    api(getState).post(`/api/v1/hashtags/${hashtagName}/unfollow`).then(() => {
      dispatch(unfollowHashtagSuccess(hashtagName));
    }).catch(error => {
      dispatch(unfollowHashtagFail(error));
    });
  };
}

export function unfollowHashtagRequest(hashtagName) {
  return {
    type: HASHTAG_UNFOLLOW_REQUEST,
    hashtagName: hashtagName,
    skipLoading: true,
  };
}

export function unfollowHashtagSuccess(hashtagName) {
  return {
    type: HASHTAG_UNFOLLOW_SUCCESS,
    hashtagName: hashtagName,
    skipLoading: true,
  };
}

export function unfollowHashtagFail(error) {
  return {
    type: HASHTAG_UNFOLLOW_FAIL,
    error,
    skipLoading: true,
  };
}
