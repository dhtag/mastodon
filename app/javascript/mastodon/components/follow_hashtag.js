import React from 'react';
import PropTypes from 'prop-types';
import Button from './button';
import { defineMessages, injectIntl } from 'react-intl';

const messages = defineMessages({
  follow: { id: 'hashtag.follow', defaultMessage: 'Follow' },
  unfollow: { id: 'hashtag.unfollow', defaultMessage: 'Unfollow' },
});

export default @injectIntl
class FollowHashtag extends React.Component {

  static defaultProps = {
    follow: false,
  };

  static propTypes = {
    name : PropTypes.string.isRequired,
    follow : PropTypes.bool,
    onFollow: PropTypes.func.isRequired,
    onUnfollow: PropTypes.func.isRequired,
    intl: PropTypes.object.isRequired,
  };

  handleUnfollow = () => {
    this.props.onUnfollow(this.props.name);
  };

  handleFollow = () => {
    this.props.onFollow(this.props.name);
  };

  render() {
    const { intl } = this.props;

    return(
      <div className='follow_button'>
        {this.props.follow ?
          ( <Button onClick={this.handleUnfollow} className='logo-button' text={intl.formatMessage(messages.unfollow)} size={20} />
          ) : (
            <Button onClick={this.handleFollow} className='logo-button' text={intl.formatMessage(messages.follow)} size={20} />)
        }
      </div>

    );
  }

}
