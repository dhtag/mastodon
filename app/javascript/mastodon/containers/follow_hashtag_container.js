import { connect } from 'react-redux';
import FollowHashtag from '../components/follow_hashtag';
import {
  followHashtag,
  unfollowHashtag,
} from '../actions/hashtags';

const mapStateToProps = (state, props) => {
  return {
    name: props.name,
    follow: state.getIn(['hashtags', 'follow_hashtag', props.name]),
  };
};

const mapDispatchToProps = (dispatch) => ({

  onFollow(hashtag_name) {
    dispatch(followHashtag(hashtag_name));
  },

  onUnfollow(hashtag_name) {
    dispatch(unfollowHashtag(hashtag_name));
  },

});

export default connect(mapStateToProps, mapDispatchToProps)(FollowHashtag);
