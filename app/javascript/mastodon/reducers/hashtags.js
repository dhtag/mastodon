import {
  HASHTAG_FOLLOW_FETCH_SUCCESS,
  HASHTAG_FOLLOW_SUCCESS,
  HASHTAG_UNFOLLOW_SUCCESS,
} from '../actions/hashtags';
import { Map as ImmutableMap } from 'immutable';

const initialState = ImmutableMap();

const setHashtagFollows = (state, hashtagFollows) => {
  return state.withMutations(map => {
    hashtagFollows.forEach(hashtagFollow => {
      map.setIn(['follow_hashtag', hashtagFollow.name], hashtagFollow.following);
    });
  });
};

export default function hashtags(state = initialState, action) {
  switch(action.type) {
  case HASHTAG_FOLLOW_FETCH_SUCCESS:
    return setHashtagFollows(state, action.hashtagFollows);
  case HASHTAG_FOLLOW_SUCCESS:
    return state.setIn(['follow_hashtag', action.hashtagName], true);
  case HASHTAG_UNFOLLOW_SUCCESS:
    return state.setIn(['follow_hashtag', action.hashtagName], false);
  default:
    return state;
  }
};
