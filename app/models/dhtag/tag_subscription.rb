# == Schema Information
#
# Table name: dhtag_tag_subscriptions
#
#  id         :bigint(8)        not null, primary key
#  account_id :bigint(8)
#  tag_id     :bigint(8)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Dhtag::TagSubscription < ApplicationRecord

  belongs_to :account, class_name: 'Account', foreign_key: :account_id
  belongs_to :tag, class_name: 'Tag', foreign_key: :tag_id
end
