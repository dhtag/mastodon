class FollowHashtagService < BaseService
  include Singleton

  def add_local_follower(account, tag_name)
    tag = Tag.find_by_name(tag_name)
    JournalService.instance.write("tag : #{tag}")
    if tag.nil?
      JournalService.instance.write('if')
      tag = Tag.create(name: tag_name)
      JournalService.instance.write("tag : #{tag}")
    end
    JournalService.instance.write('end')
    # tag_subscription = Dhtag::TagSubscription.new(account: account, tag: tag)
    tag_subscription = Dhtag::TagSubscription.new
    JournalService.instance.write('après new')
    tag_subscription.account = account
    JournalService.instance.write('après account')
    tag_subscription.tag = tag
    JournalService.instance.write("tag_subscription : #{tag_subscription}")
    # Penser peut être a intégrer des codes d'erreurs ?
    ret = tag_subscription.save
    KademliaService.signal_follow_hashtag(tag_name) if KademliaService.use?
    JournalService.instance.write("ret : #{ret}")
    return  ret
  end

  def remove_local_follower(account, tag)
    sub = find_subscription(account, tag)
    if sub.nil?
      return false
    else
      sub.destroy
    end
  end

  def remove_local_follower_with_tag_name(account, tag_name)
    tag = Tag.find_by_name(tag_name)
    if tag.nil?
      return false
    else
      remove_local_follower(account, tag)
    end
  end

  def find_subscription(account, tag)
    Dhtag::TagSubscription.where(account: account,tag: tag).take
  end

  def find_subscription_with_tag_name(account, tag_name)
    tag = Tag.find_by_name(tag_name)
    return Array.new if tag.nil?
    return find_subscription(account, tag)
  end

  def followers_of_hashtag(tag)
    tag.tag_subscriptions.map { |sub| sub.account}
  end

  def followers_of_hashtag_name(tag_name)
    tag = Tag.find_by_name(tag_name)
    if tag.nil?
      return Array.new
    else
      return followers_of_hashtag(tag)
    end
  end

  def hashtags_followed_by(account)
    account.tag_subscriptions.map { |sub| sub.tag}
  end

end
