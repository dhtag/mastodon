# frozen_string_literal: true

class HashtagInstancesQueryService < BaseService

  # Make a request to instances that possess the hashtag
  def make_query(hashtag, account_id)
    if KademliaService.use?
      PushRemoteStatusToTimelineWorker.push_bulk(find_statuses_uris(hashtag)) do |url|
        [url, account_id, "timeline:hashtag:#{hashtag.mb_chars.downcase}"]
      end
    end
  end

  private

  def find_statuses_uris(hashtag)
    instances = KademliaService.find_instances_owning(hashtag)
    s = Set.new
    instances.each do |instance|
      request = Request.new(:get, "https://#{instance}/api/v1/timelines/tag/#{hashtag}?local=true", {})
      begin
        request.perform do |res|
          s =  s + Oj.load(res.to_s, mode: :strict)
        end
      rescue => e
        JournalService.instance.write("#{e.message} on #{instance} for hashtag #{hashtag}")
      end
    end
    s.map { |el| el['uri'] }
  end
end
