# frozen_string_literal: true

class JournalService < BaseService
  include Singleton

  def write(text)
    File.open('dave_journal.log', 'a') { |file| file.write(Time.now.strftime("%d/%m/%Y %H:%M:%S") + ' :: ' + text + "\n") }
    # File.open('dave_journal.log', 'a') { |file| file.write(Time.now.strftime("%d/%m/%Y %H:%M:%S") + ' :: ' + text + "\n") }
  end
end
