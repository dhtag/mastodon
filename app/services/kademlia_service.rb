# frozen_string_literal: true

class KademliaService < BaseService

  class << self

    # Checks whether we are supposed to use the Kademlia service or not
    # Needs to be true in order for the calls to the service to work
    def use?
      Rails.configuration.kademlia['use_kademlia']
    end

    # Adds this instance to the list of instances that follow the hashtag passed as string parameter
    # Raises Mastodon::UnexpectedResponseError if the request sent to the kademlia service is not successful
    def signal_follow_hashtag(hashtag)
      options = {json: {hashtags: [hashtag]}}
      request = Request.new(:post, subscribers_uri.to_s, options)
      request.add_headers('X-Token': api_key)
      request.perform do |res|
        raise Mastodon::UnexpectedResponseError, "#{res.status} when following hashtag #{hashtag}" unless res.status.success?
      end
    end

    # Adds this instance to the list of instances that are known to have posted messages containing this hashtag string
    # Raises Mastodon::UnexpectedResponseError if the request sent to the kademlia service is not successful
    def signal_post_on_hashtag(hashtag)
      options = {json: {hashtags: [hashtag]}}
      request = Request.new(:post, owners_uri.to_s, options)
      request.add_headers('X-Token': api_key)
      request.perform do |res|
        raise Mastodon::UnexpectedResponseError, "#{res.status} when posting to hashtag #{hashtag}" unless res.status.success?
      end
    end

    # Returns the set of instances that follow the hashtag passed as string parameter
    # Raises Mastodon::UnexpectedResponseError if the request sent to the kademlia service is not successful
    def find_instances_following(hashtag)
      request = Request.new(:get, "#{subscribers_uri}/#{hashtag}", {})
      instances = Set.new
      request.add_headers('X-Token': api_key)
      request.perform do |res|
        raise Mastodon::UnexpectedResponseError, "#{res.status} when looking for instances following hashtag #{hashtag}" unless res.status.success?

        instances.merge(Oj.load(res.body, mode: :strict)['instances'])
      end
      instances
    end

    # Returns the set of instances that are known to have posted messages containing this hashtag string
    # Raises Mastodon::UnexpectedResponseError if the request sent to the kademlia service is not successful
    def find_instances_owning(hashtag)
      request = Request.new(:get, "#{owners_uri}/#{hashtag}", {})
      request.add_headers('X-Token': api_key)
      instances = Set.new
      request.perform do |res|
        raise Mastodon::UnexpectedResponseError, "#{res.status} when looking for instances owning hashtag #{hashtag}" unless res.status.success?

        instances.merge(Oj.load(res.body, mode: :strict)['instances'])
      end
      instances
    end


    private

    # Secret api key for the kademlia service
    def api_key
      Rails.configuration.kademlia['api_key']
    end

    # Points to the location keeping track of 'hashtag owners',
    # ie instances that are known to have posted a message containing a certain hashtag
    def owners_uri
      Rails.configuration.kademlia['api'] + Rails.configuration.kademlia['directlink_messages']
    end

    # Points to the location keeping track of 'hashtag subscribers',
    # ie the instances that need to get notified when a message with a certain hashtag has been posted
    def subscribers_uri
      Rails.configuration.kademlia['api'] + Rails.configuration.kademlia['directlink_subscribers']
    end
  end

end

