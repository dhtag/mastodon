json.extract! dhtag_tag_subscription, :id, :account_id, :tags_id, :created_at, :updated_at
json.url dhtag_tag_subscription_url(dhtag_tag_subscription, format: :json)
