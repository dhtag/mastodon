# frozen_string_literal: true

class PushRemoteStatusToTimelineWorker
  include Sidekiq::Worker
  include ExponentialBackoff

  sidekiq_options queue: 'pull', retry: 3

  def perform(child_url, account_id, timeline_id)
    status = FetchRemoteStatusService.new.call(child_url)
    return if status.nil?

    account = Account.find(account_id)
    message = InlineRenderer.render(status, account, :status)
    Redis.current.publish(timeline_id, Oj.dump(event: :update, payload: message, queued_at: (Time.now.to_f * 1000.0).to_i))
  rescue ActiveRecord::RecordNotFound
    true
  end
end

