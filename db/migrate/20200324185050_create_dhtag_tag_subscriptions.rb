class CreateDhtagTagSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :dhtag_tag_subscriptions do |t|
      t.belongs_to :account
      t.belongs_to :tag

      t.timestamps
    end
  end
end
