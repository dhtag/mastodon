require 'rails_helper'

describe Api::V1::Hashtags::FollowingHashtagsController do

  let(:account) { Fabricate(:account, username: 'alice') }
  let(:user)  { Fabricate(:user, account: account) }
  let(:token) { Fabricate(:accessible_access_token, resource_owner_id: user.id, scopes: 'read:accounts') }

  before do
    FollowHashtagService.instance.add_local_follower(account, 'test0')
    allow(controller).to receive(:doorkeeper_token) { token }
  end

  describe 'GET #index' do
    it 'returns http success' do
      get :index, params: { account_id: user.account.id, name: %w(test1 test0)}

      expect(response).to have_http_status(200)
    end

    it 'returns the hashtags asked for' do
      get :index, params: { account_id: user.account.id, name: %w(test1 test0)}
      json = body_as_json

      expect(json.size).to eq 2
    end

    it 'returns true for a followed hashtag' do
      get :index, params: { account_id: user.account.id, name: %w(test1 test0)}
      json = body_as_json

      expect(json.select { |tag_info| tag_info[:name].eql? 'test0'}.first[:following]).to be true
    end

    it 'returns false for a hashtag that is not followed' do
      get :index, params: { account_id: user.account.id, name: %w(test1 test0)}
      json = body_as_json

      expect(json.select { |tag_info| tag_info[:name].eql? 'test1'}.first[:following]).to be false
    end
  end
end

