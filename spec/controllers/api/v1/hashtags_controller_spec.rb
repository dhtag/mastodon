require 'rails_helper'

RSpec.describe Api::V1::HashtagsController, type: :controller do

  let(:account) { Fabricate(:account, username: 'alice') }
  let(:user)   { Fabricate(:user, account: account) }
  let(:token)  { Fabricate(:accessible_access_token, resource_owner_id: user.id) }

  before do
    allow(controller).to receive(:doorkeeper_token) { token }
  end

  describe 'POST #follow' do
    let(:hashtag_name) { 'test' }

    before do
      post :follow, params: { id: hashtag_name }
    end

    it 'returns http create' do
      expect(response).to have_http_status(201)
    end

    it 'creates a following relation between user and target hashtag' do
      expect(FollowHashtagService.instance.hashtags_followed_by(account).map(&:name)).to include hashtag_name
    end
  end

  describe 'POST #unfollow' do
    let(:hashtag_name) { 'test' }

    before do
      FollowHashtagService.instance.add_local_follower(account, hashtag_name)
      post :unfollow, params: { id: hashtag_name }
    end

    it 'returns http create' do
      expect(response).to have_http_status(201)
    end

    it 'removes following relation between user and target hashtag' do
      expect(FollowHashtagService.instance.hashtags_followed_by(account).map(&:name)).to_not include hashtag_name
    end
  end
end

