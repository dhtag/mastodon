require 'rails_helper'

RSpec.describe Dhtag::TagSubscription, type: :model do

  let!(:bob) { Fabricate(:account, username: 'bob') }
  let!(:dhtag) { Fabricate(:tag, name: 'dhtag') }

  describe "different ways to create TagSubscription : " do

    it "classic way" do
      Dhtag::TagSubscription.create!(account: bob, tag: dhtag)
    end

    it "account way" do
      bob.tag_subscriptions.create!(dhtag)
    end

    it "tag way" do
      dhtag.tag_subscriptions.create!(bob)
    end
  end

  describe "association with account" do

    before(:each) do
      @sub = Dhtag::TagSubscription.create!(account: bob, tag: dhtag)
    end

    it "should be associate with the proper account" do
      expect(@sub.account_id).to eq(bob.id)
      expect(@sub.account).to be(bob)
    end

    it "should be find from the account" do
      expect(bob.tag_subscriptions).not_to be_nil
    end
  end


  describe "association with tag" do

    before(:each) do
      @sub = Dhtag::TagSubscription.create!(account: bob, tag: dhtag)
    end

    it "should be associate with the proper tag" do
      expect(@sub.tag_id).to eq(dhtag.id)
      expect(@sub.tag).to be(dhtag)
    end

    it "should be find from the account" do
      expect(dhtag.tag_subscriptions).not_to be_nil
    end

  end
end
