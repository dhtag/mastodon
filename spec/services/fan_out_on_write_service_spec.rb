require 'rails_helper'

RSpec.describe FanOutOnWriteService, type: :service do
  let(:author)   { Fabricate(:account, username: 'tom') }
  let(:status)   { Fabricate(:status, text: 'Hello @alice #test #pony', account: author) }
  let(:alice)    { Fabricate(:user, account: Fabricate(:account, username: 'alice')).account }
  let(:follower) { Fabricate(:account, username: 'bob') }
  let(:hashtag_follower) { Fabricate(:account, username: 'cedric') }

  subject { FanOutOnWriteService.new }

  before do
    alice
    follower.follow!(author)
    FollowHashtagService.instance.add_local_follower(hashtag_follower, 'test')

    ProcessMentionsService.new.call(status)
    ProcessHashtagsService.new.call(status)

    subject.call(status)
  end

  it 'delivers status to home timeline' do
    expect(HomeFeed.new(author).get(10).map(&:id)).to include status.id
  end

  it 'delivers status to local followers' do
    pending 'some sort of problem in test environment causes this to sometimes fail'
    expect(HomeFeed.new(follower).get(10).map(&:id)).to include status.id
  end

  it 'delivers status to local hashtag followers' do
    expect(HomeFeed.new(hashtag_follower).get(10).map(&:id)).to include status.id
  end

  it 'does not deliver to non-following users' do
    expect(HomeFeed.new(alice).get(10).map(&:id)).to_not include status.id
  end

  it 'delivers status to hashtag' do
    expect(Tag.find_by!(name: 'test').statuses.pluck(:id)).to include status.id
  end

  it 'delivers status to public timeline' do
    expect(Status.as_public_timeline(alice).map(&:id)).to include status.id
  end
end
