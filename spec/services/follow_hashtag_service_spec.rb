
require 'rails_helper'

RSpec.describe FollowHashtagService, type: :service do
  subject { FollowHashtagService.instance }

  let!(:alice) { Fabricate(:account, username: 'alice') }
  let!(:bob) { Fabricate(:account, username: 'bob') }
  let!(:dhtag) { Fabricate(:tag, name: 'dhtag') }

  describe 'add_local_follower' do
    it 'by creating a new tag in db' do
      hashtag = 'yep'
      expect(Tag.find_by_name(hashtag)).to be_nil

      subject.add_local_follower(alice, hashtag)

      expect(subject.followers_of_hashtag_name(hashtag)).to include alice
      expect(subject.hashtags_followed_by(alice)).to include (Tag.find_by_name(hashtag))
    end

    it 'by finding tag in db' do
      subject.add_local_follower(alice, dhtag.name)

      expect(subject.followers_of_hashtag(dhtag)).to include alice
      expect(subject.hashtags_followed_by(alice)).to include dhtag
    end
  end

  describe 'find_subscription' do
    it 'return nil if it find nothing' do
        expect(subject.find_subscription(alice, dhtag)).to be_nil
    end

    it 'return tag_subscription line' do
      expect(subject.find_subscription(alice, dhtag)).to be_nil
      subject.add_local_follower(alice, dhtag.name)
      expect(subject.find_subscription(alice, dhtag)).to be_instance_of Dhtag::TagSubscription
      expect(subject.find_subscription(alice, dhtag).account).to eq alice
      expect(subject.find_subscription(alice, dhtag).tag).to eq dhtag
    end
  end

  describe 'find_subscription_with_tag_name' do
    it 'return nil if it find nothing' do
      expect(subject.find_subscription_with_tag_name(alice, dhtag.name)).to be_nil
    end

    it 'return tag_subscription line' do
      expect(subject.find_subscription_with_tag_name(alice, dhtag.name)).to be_nil
      subject.add_local_follower(alice, dhtag.name)
      expect(subject.find_subscription_with_tag_name(alice, dhtag.name)).to be_instance_of Dhtag::TagSubscription
      expect(subject.find_subscription_with_tag_name(alice, dhtag.name).account).to eq alice
      expect(subject.find_subscription_with_tag_name(alice, dhtag.name).tag).to eq dhtag
    end
  end

  describe 'remove_local_follower' do
    it 'return false if there is no subscription associate to account and tag' do
      expect(subject.remove_local_follower(alice, dhtag)).to eq false
    end

    it 'return the the TagSubscription which was delete' do
      expect(subject.remove_local_follower(alice, dhtag)).to eq false
      subject.add_local_follower(alice, dhtag.name)
      sub = subject.find_subscription(alice, dhtag)
      expect(subject.remove_local_follower(alice, dhtag)).to eq sub
      expect(subject.find_subscription(alice,dhtag)).to be_nil
    end
  end

  describe 'remove_local_follower_with_tag_name' do
    it 'return false if there is no subscription associate to account and tag' do
      expect(subject.remove_local_follower_with_tag_name(alice, dhtag.name)).to eq false
    end

    it 'return the the TagSubscription which was delete' do
      expect(subject.remove_local_follower_with_tag_name(alice, dhtag.name)).to eq false
      subject.add_local_follower(alice, dhtag.name)
      sub = subject.find_subscription(alice, dhtag)
      expect(subject.remove_local_follower_with_tag_name(alice, dhtag.name)).to eq sub
      expect(subject.find_subscription(alice,dhtag)).to be_nil
    end
  end

  describe 'hashtags_followed_by / followers_of_hashtag_name' do
    it 'return empty if it contains no hashtags for a new user' do
      expect(subject.hashtags_followed_by(alice)).to be_empty
    end

    it 'has no user following a new hashtag' do
      hashtag = 'oi'
      expect(subject.followers_of_hashtag_name(hashtag)).to be_empty
    end

    it 'can contain two hashtags for one user' do
      hashtag = 'yep_two'
      hashtag2 = 'yep_too'

      subject.add_local_follower(alice, hashtag)
      subject.add_local_follower(alice, hashtag2)

      expect(subject.followers_of_hashtag_name(hashtag)).to include alice
      expect(subject.followers_of_hashtag_name(hashtag2)).to include alice

      expect(subject.hashtags_followed_by(alice)).to include Tag.find_by_name(hashtag)
      expect(subject.hashtags_followed_by(alice)).to include Tag.find_by_name(hashtag2)
    end

    it 'can have two users following the same hashtag' do
      hashtag = 'yep_users'

      subject.add_local_follower(alice, hashtag)
      subject.add_local_follower(bob, hashtag)

      expect(subject.followers_of_hashtag_name(hashtag)).to include alice
      expect(subject.followers_of_hashtag_name(hashtag)).to include bob

      expect(subject.hashtags_followed_by(alice)).to include Tag.find_by_name(hashtag)

      expect(subject.hashtags_followed_by(bob)).to include Tag.find_by_name(hashtag)
    end
  end
end
