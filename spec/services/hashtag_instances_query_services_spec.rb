require 'rails_helper'

RSpec.describe HashtagInstancesQueryService, type: :service do

  let(:sender) { Fabricate(:account, username: 'alice') }

  subject { HashtagInstancesQueryService.new }

  describe 'locked account' do

    before do
      allow(KademliaService).to receive(:use?) {true}
      allow(subject).to receive(:find_statuses_uris) { %w(some_status_uri another_status_uri) }
    end

    it 'launches a worker twice' do
      Sidekiq::Testing.fake! do
        expect do
          subject.make_query('blah', 15)
        end.to change(PushRemoteStatusToTimelineWorker.jobs, :size).by(2)
      end
    end
  end
end
