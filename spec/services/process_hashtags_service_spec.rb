require 'rails_helper'

RSpec.describe ProcessHashtagsService, type: :service do
  subject { ProcessHashtagsService.new }

  context 'using kademlia' do
    before do
      allow(KademliaService).to receive(:use?) {true}
      allow(KademliaService).to receive_messages(signal_post_on_hashtag: nil)
    end

    it 'signals hashtag ownership' do
      status =  Fabricate(:status, text: 'Hello World #world #test')
      subject.call(status)

      expect(KademliaService).to have_received(:signal_post_on_hashtag).with('world')
      expect(KademliaService).to have_received(:signal_post_on_hashtag).with('test')
      expect(KademliaService).to have_received(:signal_post_on_hashtag).exactly(:twice)
    end
  end

  context 'not using kademlia' do
    before do
      allow(KademliaService).to receive(:use?) {false}
      allow(KademliaService).to receive_messages(signal_post_on_hashtag: nil)
    end

    it 'does not signal hashtag ownership' do
      status =  Fabricate(:status, text: 'Hello World #world #test')
      subject.call(status)

      expect(KademliaService).to_not have_received(:signal_post_on_hashtag)
    end
  end
end
