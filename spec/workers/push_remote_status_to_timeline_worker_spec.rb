# frozen_string_literal: true

require 'rails_helper'

describe PushRemoteStatusToTimelineWorker do
  subject { described_class.new }

  describe 'perform' do

    let(:account) { Fabricate(:account) }
    let(:status_url) { 'test_url' }
    let(:fetched_status) { Fabricate(:status) }
    let(:timeline_id) { 'test_timeline_id' }

    before do
      allow_any_instance_of(FetchRemoteStatusService).to receive(:call).with(status_url) {fetched_status}
      allow(Redis.current).to receive_messages(publish: nil)
      subject.perform(status_url, account.id, timeline_id)
    end

    it 'publishes to redis timeline' do
      expect(Redis.current).to have_received(:publish).with(timeline_id, any_args).exactly(:once)
    end
  end
end
